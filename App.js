import React from 'react';
import {SafeAreaView, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import ListProvider from './contexts/ListContext';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Provider as PaperProvider} from 'react-native-paper';
import Nav from './components/Nav';

const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
    <SafeAreaView style={{flex: 1}}>
      <PaperProvider>
        <NavigationContainer>
          <ListProvider>
            <Nav />
          </ListProvider>
        </NavigationContainer>
      </PaperProvider>
    </SafeAreaView>
  );
}
