import React, {useContext} from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import TaskScreen from '../screens/TaskScreen';
import ListScreen from '../screens/ListScreen';
import PurchaseScreen from '../screens/PurchaseScreen';
import {ListContext} from '../contexts/ListContext';

const Tab = createMaterialBottomTabNavigator();

export default function Nav() {
  const {list} = useContext(ListContext);

  return (
    <Tab.Navigator initialRouteName="Codes">
      <Tab.Screen name="Home" component={ListScreen} />
      {list.map(l => (
        <Tab.Screen
          name={l.name}
          component={l.type === 'TASK' ? TaskScreen : PurchaseScreen}
        />
      ))}
    </Tab.Navigator>
  );
}
