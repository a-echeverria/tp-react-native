import React, {useContext, useEffect, useState} from 'react';
import Form from './Form';
import {PurchaseContext} from '../../contexts/PurchaseContext';
import {View} from 'react-native';
import {Portal, Dialog, Button} from 'react-native-paper';

export default function AddEditItem({item = false}) {
  const [modal, setModal] = useState(item);
  const {addElement, editElement} = useContext(PurchaseContext);

  useEffect(() => {
    setModal(item !== false);
  }, [item]);

  const onSubmit = values => {
    if (item === false) {
      addElement(values);
    } else {
      editElement(values);
    }
  };

  return (
    <View>
      <Button onPress={() => setModal(true)}>Ajouter un Achat</Button>
      <Portal>
        <Dialog visible={modal !== false} onDismiss={() => setModal(false)}>
          <Dialog.Title>Add Purchase</Dialog.Title>
          <Dialog.Content>
            <Form onSubmit={onSubmit} selectedValue={modal} />
          </Dialog.Content>
        </Dialog>
      </Portal>
    </View>
  );
}
