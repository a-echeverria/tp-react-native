import React, {useContext} from 'react';
import ListItem from './ListItem';
import {FlatList} from 'react-native';
import {PurchaseContext} from '../../contexts/PurchaseContext';

export default function List({onEdit}) {
  const {list, fetchList, loading, page} = useContext(PurchaseContext);
  return (
    <FlatList
      data={list}
      keyExtractor={item => Date.now()}
      renderItem={({item}) => (
        <ListItem item={item} onEdit={() => onEdit(item)} />
      )}
      onRefresh={() => fetchList(undefined, true)}
      refreshing={loading}
      onEndReachedThreshold={0.7}
    />
  );
}
