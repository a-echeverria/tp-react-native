import React, {useContext} from 'react';
import ListItem from './ListItem';
import {TaskContext} from '../../contexts/TaskContext';
import {FlatList} from 'react-native';

export default function List({onEdit}) {
  const {list, fetchList, loading, page} = useContext(TaskContext);
  return (
    <FlatList
      data={list}
      keyExtractor={item => Date.now()}
      renderItem={({item}) => (
        <ListItem item={item} onEdit={() => onEdit(item)} />
      )}
      onRefresh={() => fetchList(undefined, true)}
      refreshing={loading}
      onEndReachedThreshold={0.7}
    />
  );
}
