import React, {useContext} from 'react';
import {IconButton, List} from 'react-native-paper';
import {TaskContext} from '../../contexts/TaskContext';

export default function ListItem({item, onEdit}) {
  const {deleteElement} = useContext(TaskContext);

  return (
    <List.Item
      titleStyle={{color: 'black'}}
      descriptionStyle={{color: 'black'}}
      title={item.name + ' ' + (item.status ? 'Validé' : 'Non validé')}
      description={item.description}
      right={props => (
        <>
          <IconButton
            icon="delete"
            size={20}
            color="black"
            onPress={() => deleteElement(item)}
          />
          <IconButton
            color="black"
            icon="pencil"
            size={20}
            onPress={() => onEdit(item)}
          />
        </>
      )}
    />
  );
}
