import React, {createContext, useCallback, useEffect, useState} from 'react';

import {
  addPurchase,
  deletePurchase,
  editPurchase,
  getPurchases,
} from './actions/purchases-storage';

export const PurchaseContext = createContext();

export default function PurchaseProvider({children}) {
  const [list, setList] = useState([]);
  const [ready, setReady] = useState(false);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(false);

  useEffect(() => {
    fetchList(undefined, true).then(() => setReady(true));
  }, []);

  const fetchList = useCallback(
    async (filters = {page: 1, perPage: 10}, invalidate = false) => {
      setLoading(true);
      if (!filters.perPage) {
        filters.perPage = 10;
      }
      return getPurchases(filters).then(data => {
        setLoading(false);
        setList(invalidate ? data : [...list, data]);
        setPage(filters.page);
      });
    },
    [],
  );

  const addElement = useCallback(async item => {
    const result = await addPurchase(item);
    setList([...list, result]);
  }, []);

  const deleteElement = useCallback(async item => {
    await deletePurchase(item);
    setList(list.filter(_it => _it._id !== item._id));
  }, []);

  const editElement = useCallback(async item => {
    const result = await editPurchase(item);
    setList(list.map(_it => (_it._id !== result._id ? _it : result)));
  }, []);

  const getItem = useCallback(
    id => {
      return list.find(_it => _it._id === id);
    },
    [list],
  );

  return (
    <PurchaseContext.Provider
      value={{
        list,
        fetchList,
        addElement,
        deleteElement,
        editElement,
        getItem,
        loading,
        isReady: ready,
        page,
      }}>
      {children}
    </PurchaseContext.Provider>
  );
}
