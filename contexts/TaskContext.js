import React, {createContext, useCallback, useEffect, useState} from 'react';

import {addTask, deleteTask, editTask, getTasks} from './actions/tasks-storage';

export const TaskContext = createContext();

export default function TaskProvider({children}) {
  const [list, setList] = useState([]);
  const [ready, setReady] = useState(false);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(false);

  useEffect(() => {
    fetchList(undefined, true).then(() => setReady(true));
  }, []);

  const fetchList = useCallback(
    async (filters = {page: 1, perPage: 10}, invalidate = false) => {
      setLoading(true);
      if (!filters.perPage) {
        filters.perPage = 10;
      }
      return getTasks(filters).then(data => {
        setLoading(false);
        setList(invalidate ? data : [...list, data]);
        setPage(filters.page);
      });
    },
    [],
  );

  const addElement = useCallback(async item => {
    const result = await addTask(item);
    setList([...list, result]);
  }, []);

  const deleteElement = useCallback(async item => {
    await deleteTask(item);
    setList(list.filter(_it => _it._id !== item._id));
  }, []);

  const editElement = useCallback(async item => {
    const result = await editTask(item);
    setList(list.map(_it => (_it._id !== result._id ? _it : result)));
  }, []);

  const getItem = useCallback(
    id => {
      return list.find(_it => _it._id === id);
    },
    [list],
  );

  return (
    <TaskContext.Provider
      value={{
        list,
        fetchList,
        addElement,
        deleteElement,
        editElement,
        getItem,
        loading,
        isReady: ready,
        page,
      }}>
      {children}
    </TaskContext.Provider>
  );
}
