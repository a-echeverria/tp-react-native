import AsyncStorage from '@react-native-async-storage/async-storage';

export function getPurchases() {
  return AsyncStorage.getItem('purchases').then(data => JSON.parse(data || '[]'));
}

export async function editPurchase(item) {
  let data = JSON.parse((await AsyncStorage.getItem('purchases')) || '[]');
  data = data.map(_it => (_it._id !== item._id ? _it : item));
  await AsyncStorage.setItem('purchases', JSON.stringify(data));
  return item;
}

export async function addPurchase(item) {
  let data = JSON.parse((await AsyncStorage.getItem('purchases')) || '[]');
  data = [...data, {_id: Date.now(), ...item}];
  await AsyncStorage.setItem('purchases', JSON.stringify(data));
  return item;
}
export async function deletePurchase(item) {
  let data = JSON.parse((await AsyncStorage.getItem('purchases')) || '[]');
  data = data.filter(_it => _it._id !== item._id);
  await AsyncStorage.setItem('purchases', JSON.stringify(data));
  return true;
}
