import AsyncStorage from '@react-native-async-storage/async-storage';

export function getTasks() {
  return AsyncStorage.getItem('tasks').then(data => JSON.parse(data || '[]'));
}

export async function editTask(item) {
  let data = JSON.parse((await AsyncStorage.getItem('tasks')) || '[]');
  data = data.map(_it => (_it._id !== item._id ? _it : item));
  await AsyncStorage.setItem('tasks', JSON.stringify(data));
  return item;
}

export async function addTask(item) {
  let data = JSON.parse((await AsyncStorage.getItem('tasks')) || '[]');
  data = [...data, {_id: Date.now(), ...item}];
  await AsyncStorage.setItem('tasks', JSON.stringify(data));
  return item;
}
export async function deleteTask(item) {
  let data = JSON.parse((await AsyncStorage.getItem('tasks')) || '[]');
  data = data.filter(_it => _it._id !== item._id);
  await AsyncStorage.setItem('tasks', JSON.stringify(data));
  return true;
}
