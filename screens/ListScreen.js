import React, {useState} from 'react';
import List from '../components/List/List';
import AddEditItem from '../components/List/AddEditItem';
import ListProvider from '../contexts/ListContext';

export default function ListsScreen() {
  const [selectedItem, setSelectedItem] = useState();
  const handleEditItem = item => {
    setSelectedItem(item);
  };
  return (
    <ListProvider>
      <AddEditItem item={selectedItem} />
      <List onEdit={handleEditItem} />
    </ListProvider>
  );
}
