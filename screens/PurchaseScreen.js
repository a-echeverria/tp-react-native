import React, {useState} from 'react';
import Purchase from '../components/Purchase/List';
import AddEditItem from '../components/Purchase/AddEditItem';
import PurchaseProvider from '../contexts/PurchaseContext';

export default function PurchaseScreen() {
  const [selectedItem, setSelectedItem] = useState();
  const handleEditItem = item => {
    setSelectedItem(item);
  };
  return (
    <PurchaseProvider>
      <AddEditItem item={selectedItem} />
      <Purchase onEdit={handleEditItem} />
    </PurchaseProvider>
  );
}
