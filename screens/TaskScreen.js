import React, {useState} from 'react';
import ListProvider from '../contexts/TaskContext';
import List from '../components/Task/List';
import AddEditItem from '../components/Task/AddEditItem';

export default function TasksScreen() {
  const [selectedItem, setSelectedItem] = useState();
  const handleEditItem = item => {
    setSelectedItem(item);
  };
  return (
    <ListProvider>
      <AddEditItem item={selectedItem} />
      <List onEdit={handleEditItem} />
    </ListProvider>
  );
}
